from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod

	def getFullName(self):
		pass

	def addRequest(self):
		pass

	def checkRequest(self):
		pass

	def addUser(self):
		pass

class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
# Properties
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
# Getters and Setters
	def get_firstName(self):
		print(f"First Name of Person: {self._firstName}")

	def set_firstName(self, firstName):
		self._firstName = firstName

	def get_lastName(self):
		print(f"Last Name of Person: {self._lastName}")

	def set_lastName(self, lastName):
		self._lastName = lastName

	def get_email(self):
		print(f"Email of Person: {self._email}")

	def set_email(self, email):
		self._email = email

	def get_department(self):
		print(f"Department of Person: {self._department}")

	def set_department(self, department):
		self._department = department
# Methods

	def getFullName(self):
		return (f"{self._firstName} {self._lastName}")

	def addRequest(self):
		return ("Request has been added")
# Abstact methods

	def checkRequest(self):
		print("Request still pending")

	def addUser(self):
		print(f"Action not allowed")

	def login(self):
		return (f"{self._email} has logged in")

	def logout(self):
		return (f"{self._email} has logged out")

class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
# Properties
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
		self.members = []

# Getters and Setters
	def get_firstName(self):
		print(f"First Name of Person: {self._firstName}")

	def set_firstName(self, firstName):
		self._firstName = firstName

	def get_lastName(self):
		print(f"Last Name of Person: {self._lastName}")

	def set_lastName(self, lastName):
		self._lastName = lastName

	def get_email(self):
		print(f"Email of Person: {self._email}")

	def set_email(self, email):
		self._email = email

	def get_department(self):
		print(f"Department of Person: {self._department}")

	def set_department(self, department):
		self._department = department

	def get_members(self):
		return self.members

# Methods

	def getFullName(self):
		return (f"{self._firstName} {self._lastName}")

	def addRequest(self, request):
		print(f"Request : ({request}) has been filed")
# Abstact methods

	def checkRequest(self):
		print("Request still pending")

	def addUser(self):
		print(f"Action not allowed")

	def login(self):
		return (f"{self._email} has logged in")

	def logout(self):
		return (f"{self._email} has logged out")

	def addMember(self, Employee ):
		self.members.append(Employee)

class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
# Properties
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
# Getters and Setters
	def get_firstName(self):
		print(f"First Name of Person: {self._firstName}")

	def set_firstName(self, firstName):
		self._firstName = firstName

	def get_lastName(self):
		print(f"Last Name of Person: {self._lastName}")

	def set_lastName(self, lastName):
		self._lastName = lastName

	def get_email(self):
		print(f"Email of Person: {self._email}")

	def set_email(self, email):
		self._email = email

	def get_department(self):
		print(f"Department of Person: {self._department}")

	def set_department(self, department):
		self._department = department
# Methods

	def getFullName(self):
		return (f"{self._firstName} {self._lastName}")

	def addRequest(self, request):
		print(f"Request : ({request}) has been filed")
# Abstact methods

	def checkRequest(self):
		print("Request always pending")

	def addUser(self):
		return ("User has been added")

	def login(self):
		return (f"{self._email} has logged in")

	def logout(self):
		return (f"{self._email} has logged out")


class Request():
	def __init__(self, name, requester, dateRequested):

		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = "pending"

	def set_name(self, name):
		self._name = name

	def get_name(self):
		print(f"Request name : {self._name}")

	def set_requester(self, requester):
		self._requester = requester

	def get_requester(self):
		print(f"Request requester : {self._requester}")

	def set_dateRequested(self, dateRequested):
		self._dateRequested = dateRequested

	def get_dateRequested(self):
		print(f"Request dateRequested : {self._dateRequested}")

	def set_status(self, status):
		self._status = status

	def get_status(self):
		print(f"Request status : {self._status}")

	def updateRequest(self, update):
		self._status = update

	def closeRequest(self):
		self._status = "closed"
		return (f"Request {self._name} has been closed")

	def cancelRequest(self):
		self._status = "cancelled"



# Test cases: 

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")

admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")

teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")

req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")


assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.get_members():
	print(indiv_emp.getFullName())


assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())